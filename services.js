//SERVICES
weatherApp.service('cityService', function() {
    this.city = "New York, NY";
});

weatherApp.service('weatherService', ['$resource', function($resource) {
    this.getWeather = function(city, days) {
        var weatherAPI = $resource('http://api.openweathermap.org/data/2.5/forecast/daily', { callback: "JSON_CALLBACK" }, { get: { method: "JSONP" } });
    
        return weatherAPI.get({ APPID: 'fc283735c7d38a524b181e4e6c91e120', q: city, cnt: days });
    }
}]);
